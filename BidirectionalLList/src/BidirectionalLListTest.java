import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class BidirectionalLListTest {

    private BidirectionalLList<Integer> bidirectionalLList;

    @BeforeEach
    public void setlList() {
        bidirectionalLList = new BidirectionalLList<>();
        bidirectionalLList.add(1);
        bidirectionalLList.add(2);
        bidirectionalLList.add(3);
    }

    @org.junit.jupiter.api.Test
    void clear() {
        //given

        //when
        bidirectionalLList.clear();

        //then
        assertEquals(0, bidirectionalLList.size());
    }

    @org.junit.jupiter.api.Test
    void size() {
        //given

        //when
        int actual = bidirectionalLList.size();

        //then
        assertEquals(3, actual);
    }

    @org.junit.jupiter.api.Test
    void get() {
        //given

        //when
        int actual = bidirectionalLList.get(1);

        //then
        assertEquals(2, actual);
        assertNotEquals(3, actual);
    }

    @org.junit.jupiter.api.Test
    void testAdd() {
        //given
        int number = 4;

        //when
        boolean actual = bidirectionalLList.add(number);

        //then
        assertTrue(actual);
    }

    @org.junit.jupiter.api.Test
    void testAddByIndex() {
        //given
        int index = 3, number = 5;

        //when
        boolean actualTrue = bidirectionalLList.add(index, number);

        //then
        assertTrue(actualTrue);
    }

    @org.junit.jupiter.api.Test
    void remove() {
        //given
        int expectedInt = 3;

        //when
        Integer actualInt = bidirectionalLList.remove(expectedInt);

        //then
        assertEquals(expectedInt, actualInt);
        assertNotEquals(2, actualInt);
    }

    @org.junit.jupiter.api.Test
    void removeByIndex() {
        //given
        int expectedInt = 1;
        int unexpected = 2;

        //when
        Integer actualInt = bidirectionalLList.removeByIndex(expectedInt);

        //then
        assertEquals(expectedInt, actualInt);
        assertNotEquals(unexpected, actualInt);
    }

    @org.junit.jupiter.api.Test
    void containsElement() {
        //given
        int numberOne = 3;

        //when
        boolean actualTrue = bidirectionalLList.contains(numberOne);

        //then
        assertTrue(actualTrue);
    }

    @org.junit.jupiter.api.Test
    void notContainsElement() {
        //given
        int numberTwo = 5;

        //when
        boolean actualFalse = bidirectionalLList.contains(numberTwo);

        //then
        assertFalse(actualFalse);
    }

    @org.junit.jupiter.api.Test
    void toArray() {
        //given

        //when
        Integer[] expected = {1, 2, 3};
        Integer[] actual = bidirectionalLList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals("{3}", Arrays.toString(actual));
    }

    @Test
    void subList() {
        //given
        int[] list = {1, 2, 3, 4, 5};
        BidirectionalLList lList = new BidirectionalLList(list);
        int fromIndex = 2;
        int toIndex = 3;

        //when
        int[] expected = {3, 4};
        IList newList = lList.subList(fromIndex, toIndex);
        Object[] actual = newList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(new int[]{1, 2}, actual);
    }

    @org.junit.jupiter.api.Test
    void removeAll() {
        //given

        //when
        boolean actual = bidirectionalLList.removeAll(bidirectionalLList.toArray());

        //then
        assertTrue(actual);
    }

    @org.junit.jupiter.api.Test
    void retainAll() {
        //given
        Integer[] array = {1};

        //when
        boolean actual = bidirectionalLList.retainAll(array);

        //then
        assertTrue(actual);
    }
}