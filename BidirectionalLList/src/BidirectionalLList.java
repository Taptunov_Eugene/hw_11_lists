import java.util.Arrays;

class BidirectionalLList<E> implements IList<E> {

    private class Node {
        public E data;
        public Node next;

        public Node() {
            this.data = null;
            this.next = null;
        }

        public Node(E data) {
            this.data = data;
            this.next = null;
        }

        public Node(E data, Node next) {
            this.data = data;
            this.next = next;
        }

        public void setData(E data) {
            this.data = data;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        @Override
        public String toString() {
            return "Node{data=" + data + ", next=" + next + "}";
        }
    }

    private int size;
    private Node head;

    public BidirectionalLList() {
        head = null;
        size = 0;
    }

    public BidirectionalLList(E ints) {
        addAll((E[]) ints);
    }


    private Node getNode(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public E get(int index) {
        if (index < 0)
            throw new RuntimeException("Index is wrong");
        if (index == 0)
            return head.data;

        Node current;
        if (head != null) {
            current = head.next;
            for (int i = 0; i < index - 1; i++) {
                if (current.next == null)
                    throw new RuntimeException("Index is wrong");
                current = current.next;
            }
            return current.data;
        }
        throw new RuntimeException("LList is empty");
    }

    @Override
    public boolean add(E number) {
        if (head == null) {
            head = new Node(number);
        } else {
            Node node = head;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node(number);
        }
        size++;
        return true;
    }

    @Override
    public boolean add(int index, E number) {
        Node temp = new Node(number);
        Node node = head;
        if (node != null) {
            for (int i = 0; i < index - 1 && node.next != null; i++) {
                node = node.next;
            }
        }
        assert node != null;
        temp.setNext(node.next);
        node.setNext(temp);
        size++;
        return true;
    }

    private boolean addAll(E[] array) {
        boolean flag = true;
        for (E element : array) {
            flag &= add(element);
        }
        return flag;
    }

    @Override
    public E remove(E number) {
        int temp = 0;
        Node node = head;
        while (node.data != number) {
            node = node.next;
            temp++;
        }
        removeByIndex(temp);
        return number;
    }

    @Override
    public E removeByIndex(int index) {
        if (index == 0) {
            head = head.next;
        } else {
            Node node = getNode(index - 1);
            node.next = node.next.next;
        }
        size--;
        return (E) new Integer[index];
    }

    @Override
    public boolean contains(E number) {
        Node node = head;
        if (node.data == number) {
            return true;
        }
        while (node.next != null) {
            node = node.next;
            if (node.data == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
    }

    @Override
    public E[] toArray() {
        E[] array = (E[]) new Integer[size];
        int i = 0;
        for (Node node = head; node != null; node = node.next) {
            array[i] = node.data;
            i++;
        }
        return array;
    }

    @Override
    public IList<E> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex >= size || fromIndex > toIndex) {
            throw new IndexOutOfBoundsException();
        }
        E[] array = toArray();
        int k = 0;
        Integer[] lList = new Integer[(toIndex - fromIndex) + 1];
        for (int i = fromIndex; i <= toIndex; i++) {
            lList[k++] = (Integer) array[i];
        }
        return new BidirectionalLList(lList);
    }

    @Override
    public boolean removeAll(E[] arr) {
        for (E j : arr) {
            remove(j);
        }
        return true;
    }

    @Override
    public boolean retainAll(E[] arr) {
        E[] currentArray = toArray();
        int temp = 0;
        for (E value : currentArray) {
            for (E i : arr) {
                if (value == i) {
                    temp++;
                }
            }
        }
        E[] result = (E[]) new Object[temp];
        int k = 0;
        for (E value : currentArray) {
            for (E i : arr) {
                if (value == i) {
                    result[k++] = value;
                }
            }
        }
        currentArray = result;
        clear();
        addAll(currentArray);
        return true;
    }
}