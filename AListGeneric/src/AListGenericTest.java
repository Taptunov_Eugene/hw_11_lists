import org.junit.jupiter.api.BeforeEach;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class AListGenericTest {

    private AListGeneric<Integer> aList;
    private AListGeneric<Integer> aList2;

    @BeforeEach
    public void setAList() {
        aList = new AListGeneric<>();
        aList.add(1);
        aList.add(2);
        aList.add(3);

        aList2 = new AListGeneric<>(5);
        aList2.add(5);
        aList2.add(7);
        aList2.add(14);
        aList2.add(3);
    }

    @org.junit.jupiter.api.Test
    void clear() {
        //given

        //when
        aList.clear();

        //then
        assertEquals(0, aList.size());
        assertNotEquals(2, aList.size());
    }

    @org.junit.jupiter.api.Test
    void size() {
        //given

        //when
        int actual = aList.size();
        int actual2 = aList2.size();

        //then
        assertEquals(10, actual);
        assertNotEquals(5, actual);
        assertEquals(5, actual2);
        assertNotEquals(4, actual2);
    }

    @org.junit.jupiter.api.Test
    void getByIndex() {
        //given

        //when
        int actual = aList.get(2);
        int actual2 = aList2.get(2);

        //then
        assertEquals(3, actual);
        assertNotEquals(2, actual);
        assertEquals(14, actual2);
        assertNotEquals(2, actual2);
    }

    @org.junit.jupiter.api.Test
    void testAdd() {
        //given
        int number = 4;

        //when
        boolean actual = aList.add(number);

        //then
        assertTrue(actual);
    }

    @org.junit.jupiter.api.Test
    void testAddByIndex() {
        //given
        int index = 3, number = 5;

        //when
        boolean actualTrue = aList.add(index, number);

        //then
        assertTrue(actualTrue);
    }

    @org.junit.jupiter.api.Test
    void testRemove() {
        //given
        int number = 3;

        //when
        Integer actual = aList2.remove(number);

        //then
        assertEquals(9, actual);
        assertNotEquals(2, actual);
    }

    @org.junit.jupiter.api.Test
    void removeByIndex() {
        //given
        int expectedInt = 1;
        int unexpected = 2;

        //when
        int actualInt = aList.removeByIndex(expectedInt);

        //then
        assertEquals(expectedInt, actualInt);
        assertNotEquals(unexpected, actualInt);
    }

    @org.junit.jupiter.api.Test
    void containsElement() {
        //given
        int numberOne = 3;

        //when
        boolean actualTrue = aList.contains(numberOne);

        //then
        assertTrue(actualTrue);
    }

    @org.junit.jupiter.api.Test
    void notContainsElement() {
        //given
        int numberTwo = 5;

        //when
        boolean actualFalse = aList.contains(numberTwo);

        //then
        assertFalse(actualFalse);
    }

    @org.junit.jupiter.api.Test
    void toArray() {
        //given

        //when
        Integer[] expected = {1, 2, 3};
        Integer[] actual = aList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals("{3}", Arrays.toString(actual));
    }

    @org.junit.jupiter.api.Test
    void subList() {
        //given
        AListGeneric<Integer> list = new AListGeneric<>();
        list.add(2);
        list.add(3);

        //when
        IList actual = aList.subList(1, 2);
        //int[] expected = {1, 2};
        //IList integerList = aList.subList(fromIndex, toIndex);
        //int[] actual = integerList.toArray();

        //then
        //assertEquals(list.get(0), actual.get(0));
        //assertNotEquals();
    }

    @org.junit.jupiter.api.Test
    void removeAll() {
        //given

        //when
        boolean actual = aList.removeAll(aList.toArray());

        //then
        assertTrue(actual);
    }

    @org.junit.jupiter.api.Test
    void retainAll() {
        //given
        Integer[] array = {1};

        //when
        boolean actual = aList.retainAll(array);

        //then
        assertTrue(actual);
    }
}