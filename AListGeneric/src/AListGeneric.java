import java.util.Arrays;
import java.util.Objects;

public class AListGeneric<E> implements IList<E> {

    private static final int DEFAULT_CAPACITY = 10;
    private E[] generics;
    private int size;

    public AListGeneric() {
        this.generics = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public AListGeneric(int capacity) {
        generics = (E[]) new Object[capacity];
    }

    public AListGeneric(E[] array) {
        this.generics = array;
    }

    @Override
    public void clear() {
        generics = null;
    }

    @Override
    public int size() {
        try {
            return generics.length;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public E get(int index) {
        if (index > size - 1 || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Specified index is out of range");
        }
        return generics[index];
    }

    private void ensureCapacity(int minCapacity) {
        int current = generics.length;
        if (minCapacity > current) {
            E[] newGenerics = (E[]) new Object[(int) (Math.ceil(generics.length + 1) * 1.2)];
            System.arraycopy(generics, 0, newGenerics, 0, size);
            generics = newGenerics;
        }
    }

    @Override
    public boolean add(E number) {
        if (size == generics.length) {
            ensureCapacity(size + 1);
        }
        generics[size++] = number;
        return true;
    }

    @Override
    public boolean add(int index, E number) {
        if (index > generics.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Specified index is out of range");
        }
        if (index > size - 1 && index < generics.length) {
            add(number);
        }
        ensureCapacity(size + 1);
        generics[index] = number;
        //size++;
        return true;
    }

    @Override
    public E remove(E number) {
        for (int i = 0; i < generics.length; i++) {
            if (generics[i].equals(number)) {
                removeByIndex(i);
            }
        }
        return (E) generics;
    }

    @Override
    public E removeByIndex(int index) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Specified index is out of range");
        }
        System.arraycopy(generics, index + 1, generics, index,
                generics.length - 1 - index);
        size--;
        return (E) generics;
    }

    @Override
    public boolean contains(E number) {
        for (int i = 0; i < size; i++) {
            if (generics[i] == number)
                return true;
        }

        return false;
    }

    @Override
    public void print() {
        System.out.println(Arrays.toString(toArray()));
    }

    @Override
    public E[] toArray() {
        E[] resultInts = (E[]) new Integer[size];
        System.arraycopy(generics, 0, resultInts, 0, size);
        return resultInts;
    }

    @Override
    public IList<E> subList(int fromIndex, int toIndex) {
        int j = 0;
        E[] resultInts = (E[]) new Object[(toIndex - fromIndex) + 1];
        for (int i = fromIndex; i <= toIndex; i++) {
            resultInts[j++] = generics[i];
        }
        return new AListGeneric<>(resultInts);
    }

    @Override
    public boolean removeAll(E[] arr) {
        boolean result;
        for (E j : arr) {
            remove(j);
        }
        result = arr.length > 0;
        return result;
    }

    @Override
    public boolean retainAll(E[] arr) {
        E[] result = (E[]) new Object[generics.length];
        int k = 0;
        for (E anInt : generics) {
            for (E value : arr) {
                if (anInt == value) {
                    result[k++] = anInt;
                }
            }
        }
        generics = result;
        return true;
    }
}
